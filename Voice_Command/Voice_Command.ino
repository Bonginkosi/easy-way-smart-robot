#include <AFMotor.h>
#include <Servo.h> 

char data = 0; //Variable for storing received data
int led_pin_F = 2;
int led_pin_B = 3;
int led_pin_L = 4;
int led_pin_R = 5;
int led_pin_S = 6;

int angle1 = 90;
int angle2 = 90;

AF_DCMotor motor1(3);
AF_DCMotor motor2(4);

Servo servo1;

void setup() 
{
  Serial.begin(9600);         //Sets the data rate in bits per second (baud) for serial data transmission
  pinMode(led_pin_F, OUTPUT);        //Sets digital pin 2,3 as output pin
  pinMode(led_pin_B, OUTPUT);
  pinMode(led_pin_L, OUTPUT);
  pinMode(led_pin_R, OUTPUT);
  pinMode(led_pin_S, OUTPUT);

   // turn on servo
   servo1.attach(9);
   
   
  // turn on motor #2
  motor1.setSpeed(200);
  motor1.run(RELEASE);

  motor2.setSpeed(200);
  motor2.run(RELEASE);
  
}


void loop()
{
 //--
     for (i=0; i<255; i++) 
     {
        servo1.write(i);
        delay(3);
     }
     
     for (i=255; i!=0; i--) 
     {
        servo1.write(i-255);
        delay(3);
     } 
 //-  
    if(Serial.available() > 0)  // Send data only when you receive data:
    {
        data = Serial.readString(); 

        if (data == "0")
        {   
            motor1.run(RELEASE);
            motor2.run(RELEASE);
          
        }
        else if (data == "F")
        {
           motor1.run(FORWARD);
           motor2.run(FORWARD); 
        }
    
        else if (data == "L")
        {
           motor2.run(FORWARD);
           motor1.run(BACKWARD); 
        
        else if (data == "R")
        {
           motor1.run(FORWARD);
           motor2.run(BACKWARD); 
        }
    
        else if (data == "B")
        {
           motor1.run(BACKWARD);
           motor2.run(BACKWARD);
           
        }
        else if (data == "P")
        {
           motor1.run(RELEASE);
           motor2.run(RELEASE);
           
        }
    } 
   
}
