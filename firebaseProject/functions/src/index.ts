import * as functions from 'firebase-functions';

import { dialogflow } from 'actions-on-google';
import * as admin from 'firebase-admin';

const app = dialogflow({ debug: true });

const WELCOME_INTENT = 'Default Welcome Intent';
const GOFORWARD_INTENT = 'Drive Me';
const GOBACK_INTENT = 'Turn Back';
const GOLEFT_INTENT = 'Turn Left';
const GORIGHT_INTENT = 'Turn Right';
const STOP_INTENT = 'Stop';


const serviceAccount = require('../service-account.json')
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://easy-way-c19b9.firebaseio.com'
})

// Capture Intent
// you can add a fallback function instead of a function for individual intents
app.fallback(async (conv) => {
  // intent contains the name of the intent
  // you defined in the Intents area of Dialogflow
  const intent = conv.intent;
  switch (intent) {
    case WELCOME_INTENT:
            conv.ask('Welcome! Which direction you wish to take.');
      break;
    case GOFORWARD_INTENT:
            await writeUserData('F');
            conv.ask('We Are moving in a Forward Direction.');
        break;
    case GOBACK_INTENT:
            await writeUserData('B');
            conv.ask('We are reversing.');
        break;
    case GOLEFT_INTENT:
            await writeUserData('L');
            conv.ask('We turning left');
        break;
    case GORIGHT_INTENT:
            await writeUserData('R');
            conv.ask('We turning right.');
        break;       
    case STOP_INTENT:
            //const num = conv.arguments.get(NUMBER_ARGUMENT);
            await writeUserData('P');
            conv.close(`Stopping`);
      break;
  }
});


 async function writeUserData( directionValue :String)
 {
     const directionRef = admin.database().ref();
     await directionRef.update({ Directions: directionValue })
 }

// Export the Cloud Functions
export const fulfillment = functions.https.onRequest(app);