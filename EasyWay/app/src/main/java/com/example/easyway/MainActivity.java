package com.example.easyway;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import android.widget.Toast;
import java.util.Set;
import java.util.UUID;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import java.io.IOException;
import android.os.AsyncTask;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txvDirections;
    Button btnStart, btnStop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txvDirections = (TextView)findViewById(R.id.txtDirections);
        btnStart = (Button)findViewById(R.id.btnStart);
        btnStop = (Button)findViewById(R.id.btnStop);

        btnStart.setOnClickListener(this);
        btnStop.setOnClickListener(this);


    }
    @Override
    public void  onClick(View view)
    {
        if(view == btnStart)
        {
            startService(new Intent(this,EasyWayService.class));
        }
        else if(view == btnStop)
        {
            stopService(new Intent(this,EasyWayService.class));
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();


    }




}
